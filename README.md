# Computer Environment Setup

## General Setup

The design of this repo is to have one script that will run all config editing scripts.

`setMaxWatchers.sh` : Increases maximum watchers. Needed because npm projects will not start when vscode is open because of all the 
file watchers.

* Install docker
* Install all apt packages via `sudo dpkg --set-selections <./ubuntu-files && apt-get -y update && apt-get dselect-upgrade`
* Put .bashrc and .gitconfig in your home directory

## I3 Setup

* Put i3/config in ~/.config/i3/config
    * May want to keep default keybinding for volume adjustments.
* Get VSCode environment through the account sync setting.
* All bash scripts in /usr/local/bin/
    * *brightness*: To control screen brightness. To get brightness to work you must update the line with `<root-password-here>` and put our root password there.
* *Start new terminals with current pwd*
    1. Put i3/i3_shell.sh into ~/.config/i3
    2. Add this line to your ~/.bashrc: `export PROMPT_COMMAND="pwd > /tmp/whereami"`
    3. Make sure you copied over the i3 config

## Software I use
* [Peek](https://github.com/phw/peek) is super awesome for recording quick GIFs
* [Kazam](https://github.com/hzbd/kazam) is really good for general screen recordings
