# Set up the prompt

autoload -Uz promptinit
promptinit
prompt adam1

setopt histignorealldups sharehistory

# Use emacs keybindings even if our EDITOR is set to vi
bindkey -v

# Keep 1000 lines of history within the shell and save it to ~/.zsh_history:
HISTSIZE=1000
SAVEHIST=1000
HISTFILE=~/.zsh_history

# Use modern completion system
autoload -Uz compinit
compinit

zstyle ':completion:*' auto-description 'specify: %d'
zstyle ':completion:*' completer _expand _complete _correct _approximate
zstyle ':completion:*' format 'Completing %d'
zstyle ':completion:*' group-name ''
zstyle ':completion:*' menu select=2
eval "$(dircolors -b)"
zstyle ':completion:*:default' list-colors ${(s.:.)LS_COLORS}
zstyle ':completion:*' list-colors ''
zstyle ':completion:*' list-prompt %SAt %p: Hit TAB for more, or the character to insert%s
zstyle ':completion:*' matcher-list '' 'm:{a-z}={A-Z}' 'm:{a-zA-Z}={A-Za-z}' 'r:|[._-]=* r:|=* l:|=*'
zstyle ':completion:*' menu select=long
zstyle ':completion:*' select-prompt %SScrolling active: current selection at %p%s
zstyle ':completion:*' use-compctl false
zstyle ':completion:*' verbose true

zstyle ':completion:*:*:kill:*:processes' list-colors '=(#b) #([0-9]#)*=0=01;31'
zstyle ':completion:*:kill:*' command 'ps -u $USER -o pid,%cpu,tty,cputime,cmd'

# ZSH_THEME=agnoster

# Personal Custom 
alias ll="ls -la"

precmd() { eval "$PROMPT_COMMAND" }
export PROMPT_COMMAND="pwd > /tmp/whereami"

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion

function dnames-fn {
	for ID in `docker ps | awk '{print $1}' | grep -v 'CONTAINER'`
			do
								docker inspect $ID | grep Name | head -1 | awk '{print $2}' | sed 's/,//g' | sed 's%/%%g' | sed 's/"//g'
						done
					}

				function dip-fn {
				    echo "IP addresses of all named running containers"

				        for DOC in `dnames-fn`
						    do
							            IP=`docker inspect $DOC | grep -m3 IPAddress | cut -d '"' -f 4 | tr -d "\n"`
								            OUT+=$DOC'\t'$IP'\n'
									        done
										    echo $OUT|column -t
									    }

								    function dex-fn {
								    	docker exec -it $1 ${2:-bash}
								}

							function di-fn {
								docker inspect $1
							}

						function dl-fn {
							docker logs -f $1
						}

					function drun-fn {
						docker run -it $1 $2
					}

				function dcr-fn {
					docker-compose run $@
				}

			function dsr-fn {
				docker stop $1;docker rm $1
			}

		function drmc-fn {
		       docker rm $(docker ps --all -q -f status=exited)
	       }

       function drmid-fn {
              imgs=$(docker images -q -f dangling=true)
	             [ ! -z "$imgs" ] && docker rmi "$imgs" || echo "no dangling images."
	     }

     # in order to do things like dex $(dlab label) sh
     function dlab {
	            docker ps --filter="label=$1" --format="{{.ID}}"
	    }

    function dc-fn {
            docker-compose $*
    }

function d-aws-cli-fn {
    docker run \
	               -e AWS_ACCESS_KEY_ID=$AWS_ACCESS_KEY_ID \
		                  -e AWS_DEFAULT_REGION=$AWS_DEFAULT_REGION \
				             -e AWS_SECRET_ACCESS_KEY=$AWS_SECRET_ACCESS_KEY \
					                amazon/aws-cli:latest $1 $2 $3
						}

alias daws=d-aws-cli-fn
alias dc=dc-fn
alias dcu="docker-compose up -d"
alias dcd="docker-compose down"
alias dcst="docker-compose start"
alias dcsp="docker-compose stop"
alias dcr=dcr-fn
alias dex=dex-fn
alias di=di-fn
alias dim="docker images"
alias dip=dip-fn
alias dl=dl-fn
alias dnames=dnames-fn
alias dps="docker ps"
alias dpsa="docker ps -a"
alias drmc=drmc-fn
alias drmid=drmid-fn
alias drun=drun-fn
alias dsp="docker system prune --all"
alias dsr=dsr-fn
